package main

import (
	"net"
	"net/http"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

func main() {
	type Conns struct {
		sync.Mutex
		conns map[net.Conn]string
	}
	conns := Conns{sync.Mutex{}, map[net.Conn]string{}}
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		conn, _, _, err := ws.UpgradeHTTP(r, w)
		if err != nil {
			// handle error
			println(err.Error())
			return
		}
		conns.Lock()
		conns.conns[conn] = r.URL.Query()["user"][0]
		conns.Unlock()
		go func() {
			defer func() {
				conns.Lock()
				delete(conns.conns, conn)
				conns.Unlock()
				conn.Close()
			}()
			for {
				msg, op, err := wsutil.ReadClientData(conn)
				if err != nil {
					// handle error
					println(err.Error())
					return
				}
				user := conns.conns[conn]
				for conn, _ := range conns.conns {
					err = wsutil.WriteServerMessage(conn, op, []byte(`{"user":"`+user+`","message":"`+string(msg)+`"}`))
					if err != nil {
						// handle error
					}
				}
			}
		}()
	})
	gin.SetMode(gin.ReleaseMode)
	gr := gin.Default()
	gr.GET("/chat/:user", func(ctx *gin.Context) {
		ctx.Data(200, "text/html; charset=utf-8", []byte(`
<form name="publish">
<input type="text" name="message">
<input type="submit" value="Send">
</form>
<div id="messages"></div>
<script>
	var socket = new WebSocket("ws://localhost:8080/ws?user=`+ctx.Param("user")+`");
	document.forms.publish.onsubmit = function() {
  	socket.send(this.message.value);
  	return false;
	};
	socket.onmessage = function(event) {
		var messageElem = document.createElement('DIV')
  	messageElem.innerHTML = JSON.stringify(JSON.parse(event.data), null, "..").replace(/\n/g, '<br>');
  	document.getElementById('messages').prepend(messageElem);
	}
</script>
		`))
	})
	http.HandleFunc("/", gr.ServeHTTP)
	http.ListenAndServe(":8080", nil)
}
